#!/usr/bin/lua
local utils = require 'lime.utils'
local config = require 'lime.config'
local uci = config.get_uci_cursor()

-- Describe
---- when AM grafana config is found: remove prometheus-node-push-influx config and recreate from lime-community
local section = uci:get('prometheus-node-push-influx.altermundi')
local community_section = uci:get('lime-community.grafana_am')
if section and community_section == nil then
    uci:set('lime-community', 'grafana_am', 'generic_uci_config')
    local options = {
        "prometheus-node-push-influx.altermundi=prometheus-node-push-influx",
        "prometheus-node-push-influx.altermundi.server_address=grafana.altermundi.net",
        "prometheus-node-push-influx.altermundi.server_port=8428",
        "prometheus-node-push-influx.altermundi.interval=30",
        "prometheus-node-push-influx.altermundi.disabled="
    }
    uci:set('lime-community', 'grafana_am', 'uci_set', options)
    uci:delete('lime-community', 'install_grafana_altermundi')
    uci:commit('lime-community')
    uci:delete('prometheus-node-push-influx.altermundi')
    uci:commit('prometheus-node-push-influx')
    print("AlterMundi's grafana config was found and migrated to lime-community")
end
